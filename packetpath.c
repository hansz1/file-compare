#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#define SIZE 2048

int16_t first_dif(unsigned char * block1, unsigned char * block2, uint16_t n);
int16_t file_read(void * buffer, uint16_t bytes, FILE * fp);
int16_t print_hex(unsigned char * str, int16_t);


/*** MAIN ****/
/*
  Returns: 0 succesful run
                 1 for invalid arguments
                 2 for issue opening a file
                 -1 for trouble reading from file 1
                 -2 for trouble reading from file 2
*/
int main(int argc, char ** args)
{
   /* Start clock */
   clock_t  time = clock();

   /* Handle file issues */
   if(argc != 3)
   {
      fprintf(stdout, "Please enter two file names to compare!\n");
      fprintf(stderr, "Invalid argument count!\n");
      return 1;
   }
   else
      fprintf(stdout, "Initiating process to compare two files!\n");

   FILE * fp1 = fopen(args[1],"rb");
   if ( fp1 == NULL )
   {
       perror("File 1 error");
       return 2;
   }

   FILE * fp2 = fopen(args[2],"rb");
   if ( fp2 == NULL )
   {
       perror("File 2 error");
       return 2;
   }
 
   /* Allocate arrays */ 
   unsigned char * buffer1 = (unsigned char *) malloc(SIZE);
   unsigned char * buffer2 = (unsigned char *) malloc(SIZE);

   /* Read the files */
   for( uintmax_t location = 0; ; location += SIZE )
   {
       int16_t bytes1 = file_read( buffer1, SIZE, fp1 );
       int16_t bytes2 = file_read( buffer2, SIZE, fp2 );
       if( bytes1 < 0 )
       {    
           fclose(fp1);
           fclose(fp2);
           free(buffer1);  
           free(buffer2);  
 
           return -1;
       }
       if( bytes2 < 0)
       {        
          fclose(fp1);
          fclose(fp2);
          free(buffer1);  
          free(buffer2);
 
         return -2;
       }

       int16_t position = 0; 
       if ( (position = first_dif(buffer1, buffer2, (bytes1 < bytes2) ? bytes1 : bytes2) ) >= 0 )
       {
           unsigned char str1[16];
           unsigned char str2[16];
           fprintf(stdout, "First difference located at byte %lu.\n", (uintmax_t)position + location);
         
           // inside block case             
           if( position + 16 < (bytes1 < bytes2 ? bytes1 : bytes2) )
           {
               fprintf(stdout, "\n16 bytes entirely within buffer.\n");
               memcpy( str1, buffer1 + position, 16);
               memcpy( str2, buffer2 + position, 16);
           }   
           else
           { 
               memcpy( str1, buffer1 + position, bytes1 - position );
               memcpy( str2, buffer2 + position, bytes2 - position );

               // eof case
               if( bytes1 < SIZE || bytes2 < SIZE )
               {
                    if ( bytes1 < SIZE )
                        fprintf(stdout, "\nEnd of file 1 reached\n");
                    if ( bytes2 < SIZE )
                        fprintf(stdout, "\nEnd of file 2 reached\n");
                    
               }
               //edge of block case
               else
               {
                   fprintf(stdout, "\n16 bytes split across buffers.\n");
                   int8_t str1_pos= bytes1 - position;
                   int8_t str2_pos = bytes2 - position;
                   bytes1 = file_read( buffer1, 16 - (bytes1 - position), fp1 );
                   bytes2 = file_read( buffer2, 16 - (bytes2 - position), fp2 );

                   if ( bytes2 > -1 )
                        memcpy(str2 + str2_pos, buffer2, bytes2);
                   else
                   {
                         fclose(fp1);
                         fclose(fp2);
                         free(buffer1);  
                         free(buffer2);
 
                         return -2;
                   }
                   if ( bytes1 > -1 ) 
                        memcpy(str1 + str1_pos, buffer1, bytes1);
                        
                   else
                   {
                         fclose(fp1);
                         fclose(fp2);
                         free(buffer1);  
                         free(buffer2);
 
                         return -1;
                   }
               }
           }
           print_hex(str1, 16);
           print_hex(str2, 16);
           break;                  
       }
       else if ( bytes1 != bytes2 )
       {
           fprintf(stdout, "File lengths differ, but contents identical up to the shorter file.\n");
       } 
       if( bytes1 < SIZE || bytes2 < SIZE )
       {
           break;
       }  
   }   

   /* End clock */

   time = clock() - time;

   fclose(fp1);
   fclose(fp2);

   free(buffer1);  
   free(buffer2);
   
   double sec = ((double) time) / CLOCKS_PER_SEC;
   fprintf(stdout, "%.10f seconds for the program to run.\n\n", sec);
   return 0;
}

int16_t file_read(void * buffer, uint16_t bytes, FILE * fp)
{
   uint16_t in = fread(buffer, 1, bytes, fp); 
   if( in < bytes )
   { 
     if( (feof(fp)) )
     {
        fprintf(stdout, "End of file reached.\n");
        return in;
     }
     else
     {
        perror("Issue with reading from file");
        return -1;
     }
   }
   return in;
}

int16_t print_hex(unsigned char * str, int16_t n)
{
   fprintf(stdout, "Hexadecimal:");
   for(int16_t i = 0; i < n ; ++i)
   {
      if( fprintf(stdout, " %02x ", (uint8_t) str[i]) < 0 )  
      {
          fprintf(stderr, "Issue printing hexadecimal bytes.");
          return -1;
      }
   }
   fprintf(stdout, "\n");
   return 0;
}

int16_t first_dif(unsigned char * block1, unsigned char * block2, uint16_t n)
{
   if ( memcmp(block1, block2, n) )
   {
      uint16_t position = 0;
      // search for descrepancy in chunks of decreasing size
      for( n /= 2; n; n /= 2 )
      {
          if ( !memcmp(block1, block2, n + position) )
             position += n;
      }
      return position;
   }
   else
      return -1;
}
