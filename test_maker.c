#include <stdio.h>

void writer(char * str,  int total, char * alter, int line, FILE * fp1, FILE * fp2);

int main()
{
     FILE * fp1 =   fopen("test.txt","w");
     FILE * fp2 = fopen("altered.txt", "w");

     writer("123456789\n", 100000, "ax3456789\n", 99997, fp1, fp2);     

     fclose(fp1);
     fclose(fp2);
     return 0;
}

void writer(char * str,  int total, char * alter, int line, FILE * fp1, FILE * fp2)
{
     for(int i =0; i < total; ++i)
     {
         fputs(str, fp1);
         line == i ? fputs(alter, fp2) : fputs(str, fp2);
     }
}