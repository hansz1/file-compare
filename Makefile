project = packetpath
test = test_maker

objs  = $(project).o
files = $(project).c

test_files = $(test).c
test_objs = $(test).o

CC := gcc
CFLAGS := -std=c11 -Wall -Wextra -g2 -O3

.PHONY: all

all: $(project)

$(project): $(objs)
	$(CC) $(CFLAGS) -o $(project) $(objs)

.PHONY: test
test:
	$(CC) $(CFLAGS) -o $(test) $(test_files)

.PHONY: clean
clean:
	rm -f $(project)
	rm -f $(objs)
	rm -f $(test)
	rm -f $(test_objs)